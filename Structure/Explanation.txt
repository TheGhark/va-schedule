Schedule:

The solution chosen makes use of an object called Schedule:

Schedule
- id: String
- begin: String
- end: String
- date: NSDate

The previous view controller (React) creates schedules and passes them back to SchedulesViewController (SsVC), which displays a list of all schedules created. Also, SVC allows edition of schedules by passing the selected schedule to ScheduleViewController (SVC) when performing a segue.

SsVC has only one segue called "showSchedule". Such segue displays a new SVC and a schedule is created automatically when SVC is created. When tapping the back button, the navigation controller informes its delegate, in this case SVC, that a view controller is about to be displayed. When that happens, SVC passes the recently created schedule to SsVC.

Once SsVC is displayed again, few things take place:
1. The new schedule is added
2. The table view is reloaded
3. The schedules are persisted

Note: The 3rd step could be done when the back button is tapped in SVC. However, since SsVC is responsible for displaying all schedules, it should also be in charged of triggering the load and save of schedules.

The object called ScheduleSaver (SS) only loads and saves schedules. It does so by being a property of SsVC. When a shedule is added or removed, SS saves the new schedules in the device by using NSUserDefaults. Thus, when the app is loaded for the first time, SS loads the schedules and SsVC displays them.

This solution was thought to follow a three layer archicture: View, Model and Persistance because it makes things easier to understand and responsibilities easier to separate. The view layer includes two view controllers, model one object (Schedule) and persistance is represented by SS. However, MVC is used as is default by Apple. Due to this decision, any view controller has a mixture of view logic and model logic (i.e. create next week and format dates). Nevertheless, MVC was chosen since it does not represent additional time to configure it.

The persistance layer (NSUserDefaults) was chosen for the same reason: There is no configuration required and it is accessible from any part of the app. However, it does require that a Schedule object is encodable and decodable. Such object does not have any custom properties, thus encoding and decoding are trivial (i.e. encoding/decoding strings and NSDate).

It is important to notice that persisting objects is done in the background thread. This decision was taken in order to free the UI thread of encoding/decoding objects. If there are several schedules, a loader is displayed while they are decoded.