//
//  ScheduleCell.swift
//  Structure
//
//  Created by Camilo Rodriguez Gaviria on 20/8/16.
//  Copyright © 2016 Camilo Rodriguez Gaviria. All rights reserved.
//

import Foundation
import UIKit

class ScheduleCell: UITableViewCell {
    //MARK: - Properties
    
    @IBOutlet weak var beginLabel: UILabel!
    @IBOutlet weak var endLabel: UILabel!
    
    //MARK: - Computed Properties
    
    //MARK: - Class Methods
    
    //MARK: - Initialization
    
    //MARK: - Actions
    
    //MARK: - Internal
    
    func setup(schedule schedule: Schedule) {
        beginLabel.text = "Begin: \(schedule.begin)"
        endLabel.text = "End: \(schedule.end)"
    }
    
    //MARK: - Public
    
    //MARK: - Private
    
    private func clearCell() {
        beginLabel.text = nil
        endLabel.text = nil
    }
    
    //MARK: - Overridden
    
    //MARK: - <#Delegates#>
}