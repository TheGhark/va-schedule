//
//  LoadingView.swift
//  Structure
//
//  Created by Camilo Rodriguez Gaviria on 20/8/16.
//  Copyright © 2016 Camilo Rodriguez Gaviria. All rights reserved.
//

import Foundation
import UIKit

class LoadingView: UIView {
    //MARK: - Properties
    
    private let activityIndicator = UIActivityIndicatorView(activityIndicatorStyle: .Gray)
    private var initialized = false
    
    //MARK: - Computed Properties
    
    //MARK: - Class Methods
    
    //MARK: - Initialization
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonSetup()
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        commonSetup()
    }
    
    //MARK: - Actions
    
    //MARK: - Internal
    
    func show() {
        guard hidden else { return }
        hidden = false
        activityIndicator.hidden = false
        
        activityIndicator.startAnimating()
        superview?.bringSubviewToFront(self)
        
        UIView.animateWithDuration(0.25) {
            self.alpha = 0.75
        }
    }
    
    func hide(animated animated: Bool) {
        let duration = animated ? 0.25 : 0
        
        UIView.animateWithDuration(duration, animations: {
            self.alpha = 0
            }, completion: { _ in
                self.hidden = true
                self.activityIndicator.hidden = true
                
                self.activityIndicator.stopAnimating()
                self.superview?.sendSubviewToBack(self)
        })
    }
    
    //MARK: - Public
    
    //MARK: - Private
    
    private func commonSetup() {
        guard !initialized else { return }
        initialized = true
        addActivityIndicator()
    }
    
    private func addActivityIndicator() {
        activityIndicator.translatesAutoresizingMaskIntoConstraints = false
        addSubview(activityIndicator)
        
        let centerX = NSLayoutConstraint(item: activityIndicator, attribute: .CenterX, relatedBy: .Equal, toItem: self, attribute: .CenterX, multiplier: 1, constant: 0)
        let centerY = NSLayoutConstraint(item: activityIndicator, attribute: .CenterY, relatedBy: .Equal, toItem: self, attribute: .CenterY, multiplier: 1, constant: 0)
        let height = NSLayoutConstraint(item: activityIndicator, attribute: .Height, relatedBy: .Equal, toItem: nil, attribute: .NotAnAttribute, multiplier: 1, constant: 25)
        let width = NSLayoutConstraint(item: activityIndicator, attribute: .Width, relatedBy: .Equal, toItem: nil, attribute: .NotAnAttribute, multiplier: 1, constant: 25)
        
        addConstraints([centerX, centerY, height, width])
    }
    
    //MARK: - Overridden
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        commonSetup()
    }
    
    //MARK: - <#Delegates#>
}