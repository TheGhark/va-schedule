//
//  SchedulesViewController.swift
//  Structure
//
//  Created by Camilo Rodriguez Gaviria on 20/8/16.
//  Copyright © 2016 Camilo Rodriguez Gaviria. All rights reserved.
//

import Foundation
import UIKit

private let ScheduleCellIdentifier = "ScheduleCellIdentifier"
private let ShowScheduleSegueIdentifier = "showSchedule"
private let kSchedulesKey = "schedulesKey"

class SchedulesViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {
    //MARK: - Properties
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var loadingView: LoadingView!
    private var schedules = [Schedule]()
    private let saver = ScheduleSaver()
    
    //MARK: - Computed Properties
    
    //MARK: - Class Methods
    
    //MARK: - Initialization
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        title = "Schedule"
        
        setupTableView()
        
        loadingView?.show()
        saver.loadSchedules { [unowned self] schedules in
            self.schedules = schedules ?? []
            dispatch_async(dispatch_get_main_queue()) {
                self.loadingView?.hide(animated: true)
                self.tableView.reloadData()
            }
        }
    }
    
    //MARK: - Actions
    
    @IBAction func addButtonTapped(sender: UIBarButtonItem) {
        performSegueWithIdentifier(ShowScheduleSegueIdentifier, sender: sender)
    }
    
    //MARK: - Internal
    
    func addSchedule(schedule: Schedule?) {
        guard let schedule = schedule else { return }
        
        if let index = schedules.indexOf({$0 == schedule}) {
            schedules[index] = schedule
            let indexPath = NSIndexPath(forRow: index, inSection: 0)
            tableView.reloadRowsAtIndexPaths([indexPath], withRowAnimation: .Fade)
        } else {
            schedules.append(schedule)
            tableView.reloadData()
        }
        
        saver.saveSchedules(schedules)
    }
    
    //MARK: - Public
    
    //MARK: - Private
    
    private func setupTableView() {
        tableView.rowHeight = UITableViewAutomaticDimension
        tableView.estimatedRowHeight = 70
        tableView.registerNib(UINib(nibName: "ScheduleCell", bundle: nil), forCellReuseIdentifier: ScheduleCellIdentifier)
    }
    
    private func confirmDelete(indexPath indexPath: NSIndexPath) {
        let schedule = schedules[indexPath.row]
        let alert = UIAlertController(title: "Delete schedule", message: "Are you sure you want to delete the \(schedule.description)", preferredStyle: .ActionSheet)
        
        let deleteAction = UIAlertAction(title: "Delete", style: .Destructive) { _ in
            self.removeSchedule(indexPath: indexPath)
        }
        
        let cancelAction = UIAlertAction(title: "Cancel", style: .Cancel, handler: { _ in
            self.tableView.setEditing(false, animated: true)
        })
        
        alert.addAction(deleteAction)
        alert.addAction(cancelAction)
        
        presentViewController(alert, animated: true, completion: nil)
    }
    
    private func removeSchedule(indexPath indexPath: NSIndexPath) {
        tableView.beginUpdates()
        schedules.removeAtIndex(indexPath.row)
        tableView.deleteRowsAtIndexPaths([indexPath], withRowAnimation: .Left)
        tableView.endUpdates()
        
        saver.saveSchedules(schedules)
    }
    
    //MARK: - Overridden
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if let destinationViewController = segue.destinationViewController as? ScheduleViewController {
            destinationViewController.schedule = sender as? Schedule
        }
    }
    
    //MARK: - UITableViewDataSource
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return schedules.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier(ScheduleCellIdentifier, forIndexPath: indexPath) as! ScheduleCell
        let schedule = schedules[indexPath.row]
        cell.setup(schedule: schedule)
        
        return cell
    }

    
    //MARK: - UITableViewDelegate
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        let schedule = schedules[indexPath.row]
        performSegueWithIdentifier(ShowScheduleSegueIdentifier, sender: schedule)
    }
    
    func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath) {
        if editingStyle == .Delete {
            confirmDelete(indexPath: indexPath)
        }
    }
}