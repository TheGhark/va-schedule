//
//  ScheduleViewController.swift
//  React
//
//  Created by Camilo Rodriguez Gaviria on 20/8/16.
//  Copyright © 2016 Camilo Rodriguez Gaviria. All rights reserved.
//

import Foundation
import UIKit

private typealias Dates = (begin: String, end: String)

class ScheduleViewController: UITableViewController, UINavigationControllerDelegate {
    //MARK: - Properties
    
    @IBOutlet weak var startLabel: UILabel!
    @IBOutlet weak var datePicker: UIDatePicker!
    @IBOutlet weak var endLabel: UILabel!
    
    var schedule: Schedule?
    private let dateFormatter = NSDateFormatter()
    
    //MARK: - Computed Properties
    
    //MARK: - Class Methods
    
    //MARK: - Initialization
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        title = "Schedule"
        navigationController?.delegate = self
        
        setupDateFormatter()
        setupDatePicker()
        
        if let schedule = schedule {
            update(schedule: schedule)
        } else {
            update(date: NSDate())
        }
    }
    
    //MARK: - Actions
    
    @IBAction func dateChanged(sender: UIDatePicker) {
        update(date: sender.date)
    }
    
    @IBAction func clearButtonTapped(sender: UIBarButtonItem) {
        let now = NSDate()
        datePicker.date = now
        
        update(date: now)
    }
    
    //MARK: - Internal
    
    //MARK: - Public
    
    //MARK: - Private
    
    private func formatDates(date date: NSDate?) -> Dates? {
        startLabel.text = nil
        endLabel.text = nil
        
        guard let date = date else {
            return nil
        }
        
        guard let nextWeek = self.nextWeek(date: date) else {
            return nil
        }
        
        let begin = dateFormatter.stringFromDate(date)
        startLabel.text = begin
        
        
        let end = dateFormatter.stringFromDate(nextWeek)
        endLabel.text = end
        
        return (begin: begin, end: end)
    }
    
    private func setupDateFormatter() {
        dateFormatter.dateStyle = .MediumStyle
        dateFormatter.timeStyle = .NoStyle
        dateFormatter.doesRelativeDateFormatting = true
    }
    
    private func setupDatePicker() {
        datePicker.minimumDate = NSDate()
        datePicker.datePickerMode = .Date
    }
    
    private func nextWeek(date date: NSDate) -> NSDate? {
        return NSCalendar.currentCalendar().dateByAddingUnit(.Day, value: 7, toDate: date, options: .MatchFirst)
    }
    
    private func update(date date: NSDate) {
        guard let dates = formatDates(date: date) else { return }
        
        if let schedule = schedule {
            schedule.update(begin: dates.begin, end: dates.end, date: date)
        } else {
            schedule = Schedule(begin: dates.begin, end: dates.end, date: date)
        }
    }
    
    private func update(schedule schedule: Schedule) {
        startLabel.text = schedule.begin
        endLabel.text = schedule.end
        datePicker.date = schedule.date
    }
    
    //MARK: - Overridden
    
    //MARK: - UINavigationControllerDelegate
    
    func navigationController(navigationController: UINavigationController, willShowViewController viewController: UIViewController, animated: Bool) {
        if let viewController = viewController as? SchedulesViewController {
            viewController.addSchedule(schedule)
        }
    }
}