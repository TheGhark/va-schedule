//
//  Schedule.swift
//  Structure
//
//  Created by Camilo Rodriguez Gaviria on 20/8/16.
//  Copyright © 2016 Camilo Rodriguez Gaviria. All rights reserved.
//

import Foundation

class Schedule: NSObject, NSCoding {
    //MARK: - Properties
    
    private(set) var scheduleId: String
    private(set) var begin: String
    private(set) var end: String
    private(set) var date: NSDate
    
    //MARK: - Computed Properties
    
    override var description: String {
        return "Schedule from \(begin) to \(end)"
    }
    
    //MARK: - Class Methods
    
    //MARK: - Initialization
    
    init?(begin: String?, end: String?, date: NSDate?) {
        guard let begin = begin, let end = end, let date = date where !begin.isEmpty && !end.isEmpty else { return nil }
        self.scheduleId = NSUUID().UUIDString
        self.begin = begin
        self.end = end
        self.date = date
    }
    
    //MARK: - Actions
    
    //MARK: - Internal
    
    func update(begin begin: String, end: String, date: NSDate) {
        self.begin = begin
        self.end = end
        self.date = date
    }
    
    //MARK: - Public
    
    //MARK: - Private
    
    //MARK: - Overridden
    
    //MARK: - NSCoding
    
    func encodeWithCoder(aCoder: NSCoder) {
        aCoder.encodeObject(scheduleId, forKey: "scheduleId")
        aCoder.encodeObject(begin, forKey: "begin")
        aCoder.encodeObject(end, forKey: "end")
        aCoder.encodeObject(date, forKey: "date")
    }
    
    required init?(coder aDecoder: NSCoder) {
        let scheduleId = aDecoder.decodeObjectForKey("scheduleId") as? String
        let begin = aDecoder.decodeObjectForKey("begin") as? String
        let end = aDecoder.decodeObjectForKey("end") as? String
        let date = aDecoder.decodeObjectForKey("date") as? NSDate
        
        if let scheduleId = scheduleId, begin = begin, end = end, date = date {
            self.scheduleId = scheduleId
            self.begin = begin
            self.end = end
            self.date = date
        } else {
            return nil
        }
    }
}

func ==(lhs: Schedule, rhs: Schedule) -> Bool {
    return lhs.scheduleId == rhs.scheduleId
}