//
//  ScheduleSaver.swift
//  Structure
//
//  Created by Camilo Rodriguez Gaviria on 21/8/16.
//  Copyright © 2016 Camilo Rodriguez Gaviria. All rights reserved.
//

import Foundation

class ScheduleSaver {
    //MARK: - Properties
    
    //MARK: - Computed Properties
    
    //MARK: - Class Methods
    
    //MARK: - Initialization
    
    //MARK: - Actions
    
    //MARK: - Internal
    
    func saveSchedules(schedules: [Schedule]) {
        dispatch_sync(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0)) {
            let userDefaults = NSUserDefaults.standardUserDefaults()
            let archived = NSKeyedArchiver.archivedDataWithRootObject(schedules)
            userDefaults.setValue(archived, forKey: "schedules")
            userDefaults.synchronize()
        }
    }
    
    func loadSchedules(completionHandler: ([Schedule]? -> Void)) {
        dispatch_sync(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0)) {
            let userDefaults = NSUserDefaults.standardUserDefaults()
            var schedules: [Schedule]?
            
            if let unarchived = userDefaults.valueForKey("schedules") as? NSData {
                schedules = NSKeyedUnarchiver.unarchiveObjectWithData(unarchived) as? [Schedule]
            }
            
            completionHandler(schedules)
        }
    }
    
    //MARK: - Public
    
    //MARK: - Private
    
    //MARK: - Overridden
    
    //MARK: - <#Delegates#>
}