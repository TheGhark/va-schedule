//
//  ReactViewController.swift
//  React
//
//  Created by Camilo Rodriguez Gaviria on 20/8/16.
//  Copyright © 2016 Camilo Rodriguez Gaviria. All rights reserved.
//

import Foundation
import UIKit

class ReactViewController: UITableViewController {
    //MARK: - Properties
    
    @IBOutlet weak var startLabel: UILabel!
    @IBOutlet weak var datePicker: UIDatePicker!
    @IBOutlet weak var endLabel: UILabel!
    
    private let dateFormatter = NSDateFormatter()
    
    //MARK: - Computed Properties
    
    //MARK: - Class Methods
    
    //MARK: - Initialization
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        title = "Schedule"
        setupDateFormatter()
        setupDatePicker()
        formatDates(date: NSDate())
    }
    
    //MARK: - Actions
    
    @IBAction func dateChanged(sender: UIDatePicker) {
        formatDates(date: sender.date)
    }
    
    @IBAction func clearButtonTapped(sender: UIBarButtonItem) {
        let now = NSDate()
        formatDates(date: now)
        datePicker.date = now
    }
    
    //MARK: - Internal
    
    //MARK: - Public
    
    //MARK: - Private
    
    private func formatDates(date date: NSDate?) {
        startLabel.text = nil
        endLabel.text = nil
        
        guard let date = date else {
            return
        }
        
        startLabel.text = dateFormatter.stringFromDate(date)
        
        if let nextWeek = self.nextWeek(date: date) {
            endLabel.text = dateFormatter.stringFromDate(nextWeek)
        }
    }
    
    private func setupDateFormatter() {
        dateFormatter.dateStyle = .MediumStyle
        dateFormatter.timeStyle = .NoStyle
        dateFormatter.doesRelativeDateFormatting = true
    }
    
    private func setupDatePicker() {
        datePicker.minimumDate = NSDate()
        datePicker.datePickerMode = .Date
    }
    
    private func nextWeek(date date: NSDate) -> NSDate? {
        return NSCalendar.currentCalendar().dateByAddingUnit(.Day, value: 7, toDate: date, options: .MatchFirst)
    }
    
    //MARK: - Overridden
    
    //MARK: - <#Delegates#>
}