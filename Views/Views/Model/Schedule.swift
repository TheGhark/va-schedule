//
//  Schedule.swift
//  Views
//
//  Created by Camilo Rodriguez Gaviria on 21/8/16.
//  Copyright © 2016 Camilo Rodriguez Gaviria. All rights reserved.
//

import Foundation

class Schedule {
    //MARK: - Properties
    
    private(set) var begin: String = ""
    private(set) var end: String?
    private(set) var frequency = Frequency.Once
    private(set) var date = NSDate()
    private(set) var endDate: NSDate?
    
    //MARK: - Computed Properties
    
    //MARK: - Class Methods
    
    //MARK: - Initialization
    
    init(begin: String, date: NSDate) {
        self.begin = begin
        self.date = date
    }
    
    //MARK: - Actions
    
    //MARK: - Internal
    
    func hasEnd() -> Bool {
        return end != nil
    }
    
    func update(begin begin: String, date: NSDate, end: String?, endDate: NSDate?) {
        self.begin = begin
        self.date = date
        self.end = end
        self.endDate = endDate
    }
    
    func update(frequency frequency: Frequency, end: String?, endDate: NSDate?) {
        self.frequency = frequency
        self.end = end
        self.endDate = endDate
    }
    
    func update(end end: String?, endDate: NSDate?) {
        self.end = end
        self.endDate = endDate
    }
    
    //MARK: - Public
    
    //MARK: - Private
    
    //MARK: - Overridden
    
    //MARK: - <#Delegates#>
}