//
//  Frequency.swift
//  Views
//
//  Created by Camilo Rodriguez Gaviria on 21/8/16.
//  Copyright © 2016 Camilo Rodriguez Gaviria. All rights reserved.
//

import Foundation

enum Frequency: String {
    case Once = "Once"
    case Daily = "Daily"
    case Weekly = "Weekly"
    case Monthly = "Monthly"
    
    var calendarUnit: NSCalendarUnit? {
        switch self {
        case .Once:
            return nil
        case .Daily:
            return .Day
        case .Weekly:
            return .Day
        case .Monthly:
            return .Month
        }
    }
    
    var amount: Int {
        switch self {
        case .Once:
            return 0
        case .Daily:
            return 1
        case .Weekly:
            return 7
        case .Monthly:
            return 1
        }
    }
    
    static let allValues = [Once, Daily, Weekly, Monthly]
}