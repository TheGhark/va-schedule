//
//  FrequencyCell.swift
//  Views
//
//  Created by Camilo Rodriguez Gaviria on 21/8/16.
//  Copyright © 2016 Camilo Rodriguez Gaviria. All rights reserved.
//

import Foundation
import UIKit

protocol FrequencyCellDelegate {
    func frequencyCell(frequencyCell: FrequencyCell, didChangeFrequency frequency: Frequency)
}

class FrequencyCell: UITableViewCell, UIPickerViewDataSource, UIPickerViewDelegate {
    //MARK: - Properties
    
    @IBOutlet weak var pickerView: UIPickerView!
    
    private var delegate: FrequencyCellDelegate?
    
    private let frequencies = Frequency.allValues
    
    //MARK: - Computed Properties
    
    //MARK: - Class Methods
    
    //MARK: - Initialization
    
    //MARK: - Actions
    
    //MARK: - Internal
    
    func setup(frequency frequency: Frequency, delegate: FrequencyCellDelegate?) {
        if let index = frequencies.indexOf(frequency) {
            pickerView.selectRow(index, inComponent: 0, animated: true)
        }
        
        self.delegate = delegate
    }
    
    //MARK: - Public
    
    //MARK: - Private
    
    //MARK: - Overridden
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        pickerView.dataSource = self
        pickerView.delegate = self
    }
    
    //MARK: - UIPickerViewDataSource
    
    func numberOfComponentsInPickerView(pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return frequencies.count
    }
    
    //MARK: - UIPickerViewDelegate
    
    func pickerView(pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        let frequency = frequencies[row]
        return frequency.rawValue
    }
    
    func pickerView(pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        let frequency = frequencies[row]
        delegate?.frequencyCell(self, didChangeFrequency: frequency)
    }
}