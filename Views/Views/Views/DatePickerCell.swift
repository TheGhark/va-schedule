//
//  DatePickerCell.swift
//  Views
//
//  Created by Camilo Rodriguez Gaviria on 21/8/16.
//  Copyright © 2016 Camilo Rodriguez Gaviria. All rights reserved.
//

import Foundation
import UIKit

protocol DatePickerCellDelegate: class {
    func datePickerCell(datePickerCell: DatePickerCell, didChangeDate date: NSDate)
}

class DatePickerCell: UITableViewCell {
    //MARK: - Properties
    
    @IBOutlet weak var datePicker: UIDatePicker!
    
    private weak var delegate: DatePickerCellDelegate?
    
    //MARK: - Computed Properties
    
    //MARK: - Class Methods
    
    //MARK: - Initialization
    
    //MARK: - Actions
    
    @IBAction func dateChanged(sender: UIDatePicker) {
        delegate?.datePickerCell(self, didChangeDate: sender.date)
    }
    
    //MARK: - Internal
    
    func setup(minimumDate minimumDate: NSDate?, delegate: DatePickerCellDelegate?) {
        datePicker.minimumDate = minimumDate ?? NSDate()
        self.delegate = delegate
    }
    
    //MARK: - Public
    
    //MARK: - Private
    
    //MARK: - Overridden
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        datePicker.datePickerMode = .Date
    }
    
    //MARK: - <#Delegates#>
}