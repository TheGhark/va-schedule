//
//  ScheduleVeiwController.swift
//  Views
//
//  Created by Camilo Rodriguez Gaviria on 21/8/16.
//  Copyright © 2016 Camilo Rodriguez Gaviria. All rights reserved.
//

import Foundation
import UIKit

private enum CellType: Int {
    case Begin = 0
    case End = 1
    case Frequency = 2
    case DatePicker = 3
    case FrequencyPicker = 4
}

private let DetailCellIdentifier = "DetailCellIdentifier"
private let DatePickerCellIdentifier = "DatePickerCellIdentifier"
private let FrequencyCellIdentifier = "FrequencyCellIdentifier"

class ScheduleVeiwController: UITableViewController, DatePickerCellDelegate, FrequencyCellDelegate {
    //MARK: - Properties
    
    private var types = [CellType.Begin, CellType.Frequency]
    private var choosingBegin = true
    
    private var schedule: Schedule!
    private let dateFormatter = NSDateFormatter()
    
    //MARK: - Computed Properties
    
    //MARK: - Class Methods
    
    //MARK: - Initialization
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        title = "Schedule"
        setupTableView()
        setupDateFormatter()
        initializeSchedule()
    }
    
    //MARK: - Actions
    
    @IBAction func clearButtonTapped(sender: UIBarButtonItem) {
        initializeSchedule()
        tableView.beginUpdates()
        
        var toKeep = [NSIndexPath]()
        var toRemove = [NSIndexPath]()
        
        for (index, type) in types.enumerate() {
            let indexPath = NSIndexPath(forRow: index, inSection: 0)
            
            switch type {
            case .Begin, .Frequency:
                toKeep.append(indexPath)
                types.append(type)
            default:
                toRemove.append(indexPath)
            }
        }
        
        types = [.Begin, .Frequency]
        tableView.deleteRowsAtIndexPaths(toRemove, withRowAnimation: .Left)
        tableView.reloadRowsAtIndexPaths(toKeep, withRowAnimation: .None)
        tableView.endUpdates()
    }
    
    //MARK: - Internal
    
    //MARK: - Public
    
    //MARK: - Private
    
    private func initializeSchedule() {
        let now = NSDate()
        let begin = dateFormatter.stringFromDate(now)
        schedule = Schedule(begin: begin, date: now)
    }
    
    private func setupDateFormatter() {
        dateFormatter.dateStyle = .MediumStyle
        dateFormatter.timeStyle = .NoStyle
        dateFormatter.doesRelativeDateFormatting = true
    }
    
    private func setupTableView() {
        tableView.rowHeight = UITableViewAutomaticDimension
        
        tableView.registerNib(UINib(nibName: "DetailCell", bundle: nil), forCellReuseIdentifier: DetailCellIdentifier)
        tableView.registerNib(UINib(nibName: "DatePickerCell", bundle: nil), forCellReuseIdentifier: DatePickerCellIdentifier)
        tableView.registerNib(UINib(nibName: "FrequencyCell", bundle: nil), forCellReuseIdentifier: FrequencyCellIdentifier)
    }
    
    private func indexPathForType(type: CellType) -> NSIndexPath? {
        if let index = types.indexOf({ $0.rawValue == type.rawValue }) {
            return NSIndexPath(forRow: index, inSection: 0)
        }
        
        return nil
    }
    
    private func select(type type: CellType) {
        switch type {
        case .Begin:
            choosingBegin = true
            
            if !schedule.hasEnd() {
                types = [.Begin, .DatePicker, .Frequency]
            } else {
                types = [.Begin, .DatePicker, .Frequency, .End]
            }
            
            tableView.reloadData()
            
        case .End:
            choosingBegin = false
            types = [.Begin, .Frequency, .End, .DatePicker]
            tableView.reloadData()
            
        case .Frequency:
            choosingBegin = false
            
            if !schedule.hasEnd() {
                types = [.Begin, .Frequency, .FrequencyPicker]
            } else {
                types = [.Begin, .Frequency, .FrequencyPicker, .End]
            }
            
            tableView.reloadData()
        default: break
        }
    }
    
    private func update(type type: CellType) {
        switch type {
        case .Begin:
            choosingBegin = true
            
            if !schedule.hasEnd() {
                types = [.Begin, .Frequency]
            } else {
                types = [.Begin, .Frequency, .End]
            }
            
            tableView.reloadData()
            
        case .End:
            choosingBegin = false
            types = [.Begin, .Frequency, .End]
            tableView.reloadData()
            
        case .Frequency:
            choosingBegin = false
            
            if !schedule.hasEnd() || schedule.frequency == .Once {
                types = [.Begin, .Frequency]
            } else {
                types = [.Begin, .Frequency, .End]
            }
            
            tableView.reloadData()
        default: break
        }
    }
    
    private func calculateEnd(frequency frequency: Frequency, date: NSDate) -> (end: String, date: NSDate)? {
        guard let calendarUnit = frequency.calendarUnit else { return nil }
        
        if let date = NSCalendar.currentCalendar().dateByAddingUnit(calendarUnit, value: frequency.amount, toDate: date, options: NSCalendarOptions.MatchFirst) {
            return (end: dateFormatter.stringFromDate(date), date: date)
        }
        
        return nil
    }
    
    //MARK: - Overridden
    
    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return types.count
    }
    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let type = types[indexPath.row]
        
        switch type {
        case .Begin, .End, .Frequency:
            let cell = tableView.dequeueReusableCellWithIdentifier(DetailCellIdentifier, forIndexPath: indexPath) as! DetailCell
            
            if type == .Begin {
                cell.setup(title: "Begin:", time: schedule.begin)
            } else if type == .End {
                cell.setup(title: "End:", time: schedule.end)
            } else {
                cell.setup(title: "Frequency:", time: schedule.frequency.rawValue)
            }
            
            cell.selectionStyle = .None
            
            return cell
            
        case .DatePicker:
            let cell = tableView.dequeueReusableCellWithIdentifier(DatePickerCellIdentifier, forIndexPath: indexPath) as! DatePickerCell
            let date = choosingBegin ? schedule.date : schedule.endDate
            cell.setup(minimumDate: date, delegate: self)
            cell.selectionStyle = .None
            
            return cell
            
        case .FrequencyPicker:
            let cell = tableView.dequeueReusableCellWithIdentifier(FrequencyCellIdentifier, forIndexPath: indexPath) as! FrequencyCell
            cell.setup(frequency: schedule.frequency, delegate: self)
            cell.selectionStyle = .None
            
            return cell
        }
        
    }
    
    override func tableView(tableView: UITableView, estimatedHeightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        let type = types[indexPath.row]
        
        switch type {
        case .Begin, .End, .Frequency:
            return 44
        default:
            return 215
        }
    }
    
    override func tableView(tableView: UITableView, willSelectRowAtIndexPath indexPath: NSIndexPath) -> NSIndexPath? {
        let type = types[indexPath.row]
        
        switch type {
        case .DatePicker, .FrequencyPicker:
            return nil
        default:
            return indexPath
        }
    }
    
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        let type = types[indexPath.row]
        select(type: type)
    }
    
    //MARK: - DatePickerCellDelegate
    
    func datePickerCell(datePickerCell: DatePickerCell, didChangeDate date: NSDate) {
        if choosingBegin {
            let begin = dateFormatter.stringFromDate(date)
            let endInfo = calculateEnd(frequency: schedule.frequency, date: date)
            schedule.update(begin: begin, date: date, end: endInfo?.end, endDate: endInfo?.date)
            update(type: .Begin)
        } else {
            let end = dateFormatter.stringFromDate(date)
            schedule.update(end: end, endDate: date)
            update(type: .End)
        }
    }
    
    //MARK: - FrequencyCellDelegate
    
    func frequencyCell(frequencyCell: FrequencyCell, didChangeFrequency frequency: Frequency) {
        let endInfo = calculateEnd(frequency: frequency, date: schedule.date)
        schedule.update(frequency: frequency, end: endInfo?.end, endDate: endInfo?.date)
        update(type: .Frequency)
    }
}